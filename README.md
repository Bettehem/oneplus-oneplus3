# Ubuntu Touch device tree for the OnePlus 3/3T

This is based on Halium 9.0, and uses the mechanism described in [this
page](https://github.com/ubports/porting-notes/wiki/GitLab-CI-builds-for-devices-based-on-halium_arm64-(Halium-9)).

This project can be built manually (see the instructions below)

## How to build

To manually build this project, follow these steps:

```bash
export HOSTCC=gcc-9  # the build breaks with gcc-11
./build.sh -b bd  # bd is the name of the build directory
./build/prepare-fake-ota.sh out/device_violet.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command out
```


## Install

After the build process has successfully completed, run

```bash
fastboot flash boot out/boot.img
fastboot flash system out/system.img
```

## Building the vendor image

If you want to build a vendor image, the steps are quite similar to those needed
to build the system image with Halium:
1. Follow instructions for building here: https://github.com/OP3-Halium/Documentation#install-prerequisites-for-building
2. Run `mka vendorimage`
This will generate a file `our/target/product/oneplus3/vendor.img` that can be
flashed with `fastboot flash vendor vendor.img`.
